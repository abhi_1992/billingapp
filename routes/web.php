<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/','Auth\LoginController@login');
Route::get('/home', 'HomeController@index')->name('home');
//user mgt
Route::get('user_mgt','UserController@userList');
Route::get('add-user','UserController@addUser');
Route::post('add-user','UserController@saveUser');
Route::get('edit-user','UserController@ediUser');
Route::put('update-user/{id}','UserController@updateUser');
Route::get('delete-user/{id}','UserController@deletUser');
//2 factor authentication routes
Route::get('/complete-registration', 'Auth\RegisterController@completeRegistration');
Route::post('/2fa', function () {
    return redirect(URL()->previous());
})->name('2fa')->middleware('2fa');
Route::get('/2fa',['as' => 'dashboard',     'uses' => 'HomeController@index']);
Route::get('/re-authenticate', 'HomeController@reauthenticate');

//enable and disable 2fa
Route::get('enable', 'Google2FAController@enableTwoFactor');
Route::get('disable', 'Google2FAController@disableTwoFactor');

//user
Route::get('users', function () {
    return view('users.add_user');
});

