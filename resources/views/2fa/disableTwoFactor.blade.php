@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div><br><br>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-heading">Two-Factor Authentication</div>
                            <div class="panel-body">
                                2FA has been removed
                                <br /><br />
                                <a href="{{ url('/home') }}">Go Home</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php  $user = \Auth::user();
                echo "<pre>"; print_r($user);?>
            </div>
        </div>
    </div>
</div>
@endsection
