@extends('layouts.app')
@section('title', 'Edit user')
@section('content')
<style>
    .error{
        color:red;
    }
</style>
<section class="content-header">
      <h1>
          Edit User
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('user_mgt')}}"><i class="fa fa-dashboard"></i>User</a></li>
        <li class="active">Edit User</li>
      </ol>
    </section>
<section class="content">
<div class="row">
 <div class="col-md-12">
          <div class="box" style="border-top: 3px solid #ffffff;">
            <div class="box-header">
              <h3 class="box-title"></h3>
            </div>
            {!! Form::model($users,[
                        'method' => 'PUT',
                        'url' => ['update-user',$users->id],
                        'class'=> 'form-horizontal',
                        'id'=>'orderForm'
                    ]) !!}
                {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="userName" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-4">
                      <input type="text" class="form-control" id="userName" placeholder="Name" name="name" value="{{$users->name}}" required>
                  </div>
                   <label for="company" class="col-sm-2 control-label">Organization</label>

                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="company" placeholder="Organization" name="organization" value="{{$users->organization}}" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="gst" class="col-sm-2 control-label">GST No'</label>

                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="gst" placeholder="GST" name="gst_no" value="{{$users->gst_no}}" required>
                  </div>
                  <label for="gst" class="col-sm-2 control-label">PAN No'</label>

                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="pan_card" placeholder="PAN" name="pan_no" value="{{$users->pan_no}}" required>
                  </div>
                </div>
                 <div class="form-group">
                  <label class="col-sm-2 control-label">Address</label>  
                    <div class="col-sm-4">
                    <textarea class="form-control" rows="3" placeholder="Enter Address..." name="address">{{$users->address}}</textarea>   
                    </div>
                   <label for="gst" class="col-sm-2 control-label">Contact No'</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="contact" placeholder="Contact No" name="contact_no" value="{{$users->contact_no}}" required>
                  </div>
                </div>
                  <div class="form-group">
                   <label for="gst" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-4">
                      <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="{{$users->email}}" required>
                  </div> 
                       <label for="gst" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-4">
                    <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                  </div>
                  </div>
                  <?php $type=$users->type;

                  ?>
                  <div class="form-group">
                <label class="col-sm-2 control-label">Type</label>
                <div class="col-sm-4">
                <select class="form-control select2" style="width: 100%;" name="type" required>
                 <option value="">-- Select Type -- </option>
                  @foreach($type_data as $d)
                 <option value="{{$d->type_id}}"  <?php if($d->type_id==$type) echo "selected"; ?>>{{$d->type_name}}</option>
                 @endforeach
                </select>
                </div>
                          <label class="col-sm-2 control-label">Category</label>
                <div class="col-sm-4">
                <select class="form-control select2" style="width: 100%;" name="category" required>
                  @foreach($category_data as $d)
                 <option value="{{$d->cat_id}}"  <?php if($d->cat_id==$users->category) echo "selected"; ?>>{{$d->cat_name}}</option>
                 @endforeach
                </select>
                </div>
              </div>
                  
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-success">Update</button>
                  <a href="{{url('user_mgt')}}" class="btn btn-danger" >Cancel</a>
              </div>
            </form>
          </div>
        </div>   
</div>
  </section> 
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/select2/dist/js/select2.full.min.js"></script>
<script>
 $(document).ready(function(){
    $('.select2').select2() 
 });
</script>
@endsection
