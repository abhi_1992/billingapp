@extends('layouts.app')

@section('content')
<link href="css/sweetalert.css" rel="stylesheet">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div><br><br>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-heading">Two-Factor Authentication</div>

                            <div class="panel-body">
                                @if (Auth::user()->google2fa_secret)
                                <a  class="btn btn-warning disable" onclick="disable();">Disable 2FA</a>
                                @else
                                <a href="{{ url('enable') }}" class="btn btn-primary">Enable 2FA</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/sweetalert.min.js"></script>
<script>
    function disable()
    {
        $.ajax({
        url: 'disable',
        type: "get",
        success: function(reportdata) { 
                console.log(reportdata);
                swal({
                    position: 'top-end',
                    type: 'success',
                    title: '2FA disabled',
                    showConfirmButton: true,
                    }, function(isConfirm){
                     if (isConfirm) {
                        location.reload(true);
                     }
                     });  
        }
    });
    }
    </script>
@endsection
