<aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="image" style="text-align: center;">
          <img src="dist/img/logo.png" class="" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>iPing Data Labs</p>
        </div>
      </div>
      
    <ul class="sidebar-menu">
        <li class="active treeview">
          <a href="{{url('home')}}">
            <i class="fa fa-dashboard"></i> <span>Home</span> 
          </a>
        </li>
          <li class="treeview">
          <a href="{{url('user_mgt')}}">
            <i class="fa fa-user"></i>
            <span>User Management</span>
          </a>
              
        </li>
    </ul>
        
        
    </section>
</aside>