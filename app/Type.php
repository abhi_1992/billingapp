<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $primaryKey = "type_id";
    public $table = "tbl_type";
    public $timestamps=true;
    protected $fillable = [
        'type_name','user_id','is_active'
    ];
}
