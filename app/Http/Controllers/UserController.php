<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Operator;
use App\SubDepartment;
use Session;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
            $this->department = json_decode(Auth::user()->dept_id,true);
           return $next($request);
        });
    }
    
  
    
    public function userList()
    {
        $users = \App\BusinessUser::where('is_active','=',0)->get();
            return view('auth.user_list',['users'=>$users]);
    }
    
    public function addUser()
    {
        $type =\App\Type::get();
        $category = \App\Category::get();
            return view('auth.add_user',['type_data'=>$type,'category_data'=>$category]);
    }
    
    public function saveUser(Request $request)
    {
        $requestData = $request->all();
       
        $requestData['password'] = bcrypt($requestData['password']);
        $requestData['owner'] = Auth::user()->id;
        \App\BusinessUser::create($requestData);
        Session::flash('alert-success', 'Created Successfully.');
        return redirect('user_mgt');
    }
    
    public function ediUser()
    {
        $id = $_GET['id'];
        
            $users = \App\BusinessUser::findorfail($id);
            $type =\App\Type::get();
            $category = \App\Category::get();
            return view('auth.edit_user',['users'=>$users,'type_data'=>$type,'category_data'=>$category]);
    }
    
    public function updateUser($id,Request $request)
    {
        $requestData = $request->except('password');
        if ($request->password)
            $requestData['password'] = bcrypt($request->password);
         $requestData['owner'] = Auth::user()->id;
        $users = \App\BusinessUser::findorfail($id);
        $users->update($requestData);
        Session::flash('alert-success', 'Updated Successfully.');
        return redirect('user_mgt');
    }
    
    public function deletUser($id)
    {
        $query= \App\BusinessUser::where('id', $id)->update(['is_active' => 1]);
        Session::flash('alert-success', 'Deleted Successfully.');
        return redirect('user_mgt');
    }
    
  
   
}
